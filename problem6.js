const users = require('./users');
function metricgender(users) {

    let MaleArray = [];
    let FemaleArray = [];
    let PolygenderArray = [];
    let BigenderArray = [];
    let GenderqueerArray = [];
    let GenderfluidArray = [];
    let AgenderArray = [];
    for (let i = 0; i < users.length; i++) {
        if (users[i].gender === 'Male') {
            MaleArray.push(users[i]);
        } else if (users[i].gender === 'Female') {
            FemaleArray.push(users[i]);
        } else if (users[i].gender === 'Polygender') {
            PolygenderArray.push(users[i]);
        } else if (users[i].gender === 'Bigender') {
            BigenderArray.push(users[i]);
        } else if (users[i].gender === 'Genderfluid') {
            GenderfluidArray.push(users[i]);
        } else if (users[i].gender === 'Genderqueer') {
            GenderqueerArray.push(users[i]);
        } else if (users[i].gender === 'Agender') {
            AgenderArray.push(users[i]);
        }

    }
    const result = [
        MaleArray,
        FemaleArray,
        PolygenderArray,
        BigenderArray,
        GenderfluidArray,
        GenderqueerArray,
        AgenderArray
    ];


    return JSON.stringify(result);


}

module.exports=metricgender;

// console.log(metricgender(users));