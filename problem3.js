// const users = require("./users.js");
// console.log(users);

function sortingArray(users){
    for (let i=0; i<users.length; i++) {
        for (let j=i+1;j<users.length;j++){
            if (users[i].last_name>users[j].last_name){
                let temporary=users[i];
                users[i]=users[j];
                users[j]=temporary;
            }
        }
    }
    return users;
}
module.exports=sortingArray;